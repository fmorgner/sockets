#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>

char* acIPAddress = "::1";
uint16_t nTCPPort = 50000;

void* handleConnection(void* argument)
  {
  int nClientSocket = *(int*)argument;
  char acBytes[5000];

  for(int nIteration = 0; nIteration < 5000; nIteration++)
    {
    acBytes[nIteration] = (char)(nIteration % 10) + 48;
    }
  write(nClientSocket, acBytes, sizeof(acBytes));


  shutdown(nClientSocket, SHUT_WR);
  close(nClientSocket);

  return 0;
  }

int main(int argc, char** argv)
  {
  int nSocket = socket(PF_INET6, SOCK_STREAM, 6); // 6 is TCP

  if(nSocket == -1)
    {
    perror("Could not create socket!");
    return EXIT_FAILURE;
    }

  struct sockaddr_in6 sAddress;
  sAddress.sin6_family = AF_INET6;
  sAddress.sin6_port = htons(nTCPPort);
  sAddress.sin6_addr = in6addr_any;
//  inet_pton(AF_INET6, acIPAddress, &sAddress.sin6_addr);

  int nBindState = bind(nSocket, (struct sockaddr*)&sAddress, sizeof sAddress);

  if(nBindState == -1)
    {
    perror("Could not bind");
    return EXIT_FAILURE;
    }

  printf("--- Listening on %s port %d for connections\n", acIPAddress, nTCPPort);

  listen(nSocket, 5);

  struct sockaddr_storage sClientAddress;
  socklen_t nClientAddressSize = sizeof sClientAddress;

  pthread_t workerThread;

  while(1)
    {
    int nClientSocket = accept(nSocket, (struct sockaddr*)&sClientAddress, &nClientAddressSize);

    pthread_create(&workerThread, NULL, &handleConnection, &nClientSocket);
    }

  return EXIT_SUCCESS;
  }

