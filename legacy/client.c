#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <netdb.h>

void resolveHostname(char* acHostname, struct hostent** sHostEntry)
  {
  if(acHostname == NULL)
    {
    fprintf(stderr, "!!! FATAL: NULL hostname provided!\n");
    exit(EXIT_FAILURE);
    }

  if(sHostEntry == NULL)
    {
    fprintf(stderr, "!!! FATAL: NULL pointer for host entry provided!\n");
    }
  else
    {
    *sHostEntry = gethostbyname2(acHostname, AF_INET6);
    }

  if(sHostEntry == NULL)
    {
    fprintf(stderr, "!!! FATAL: Could not resolve name '%s'\n", acHostname);
    exit(EXIT_FAILURE);
    }
  }

int main(int argc, char** argv)
  {
  uint16_t nTCPPort = 50000;

  int nSocket = socket(PF_INET6, SOCK_STREAM, 6); // 6 is TCP

  if(nSocket == -1)
    {
    perror("!!! Fatal: Could not create socket");
    return EXIT_FAILURE;
    }

  struct hostent* sHostEntry;
  resolveHostname(argv[1], &sHostEntry);

  struct sockaddr_in6 sAddress;
  sAddress.sin6_family = AF_INET6;
  sAddress.sin6_port = htons(nTCPPort);
  memcpy((char*)&sAddress.sin6_addr, sHostEntry->h_addr_list[0], sHostEntry->h_length);

  int nConnectState = connect(nSocket, (struct sockaddr*)&sAddress, sizeof sAddress);

  if(nConnectState == -1)
    {
    perror("!!! Fatal: Could not connect");
    return EXIT_FAILURE;
    }

  char message[128] = {0};
  int nBytes = 0;
  int nBytesTotal = 0;

  while((nBytes = recv(nSocket, message, sizeof(message), 0)) > 0)
    {
    printf("NNN Note: Received '%s'\n", message);
    nBytesTotal += nBytes;
    }

  printf("III Info: Received a total of %d bytes.\n", nBytesTotal);

  close(nSocket);

  return EXIT_SUCCESS;
  }

