#ifndef __SOCKET_H
#define __SOCKET_H

#include <string>
#include <sys/socket.h>

namespace hsr
  {

  class Socket
    {
    private:
      int m_nSocketDescriptor = -1;
      int m_nProtocol = -1;
      int m_nDomain = -1;

      struct sockaddr_storage m_sAddress;

      Socket();

    protected:
      bool ResolveName(const std::string &roHostname);

    public:
      Socket(std::string oAddressFamily, std::string oProtocol, std::string oHostname, int nPort);
    };

  };

#endif // __SOCKET_H

