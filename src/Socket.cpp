#include "Socket.h"
#include <netdb.h>
#include <stdexcept>
#include <memory>
#include <cstring>

hsr::Socket::Socket(std::string oAddressFamily, std::string oProtocol, std::string oHostname, int nPort)
  {
  m_sAddress.ss_family = AF_INET6;
  ResolveName(oHostname);
  }

bool hsr::Socket::ResolveName(const std::string& roHostname)
  {
  if(roHostname == "")
    {
    throw(std::runtime_error("Empty hostname"));
    }

  struct hostent* sHostEntry(gethostbyname2(roHostname.c_str(), m_sAddress.ss_family));
  bool bCouldResolve = (sHostEntry != NULL);

  if(bCouldResolve)
    {
    if(m_sAddress.ss_family == AF_INET6)
      {
      memcpy((char*)&(((struct sockaddr_in6*)&m_sAddress)->sin6_addr), sHostEntry->h_addr_list[0], sHostEntry->h_length);
      }
    else
      {
      memcpy((char*)&(((struct sockaddr_in*)&m_sAddress)->sin_addr), sHostEntry->h_addr_list[0], sHostEntry->h_length);
      }
    }

  return bCouldResolve;
  }

